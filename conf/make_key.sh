#!/bin/bash

PRIVATE_KEY="private_key.pem"
PRIVATE_DER="private_key.der"
PUBLIC_DER="public_key.der"

BITDEPTH=1024

cd `dirname $0`

if [ ! -f $PRIVATE_KEY ]
  then
	openssl genrsa -out $PRIVATE_KEY $BITDEPTH
fi

openssl pkcs8 -topk8 -inform PEM -outform DER -in $PRIVATE_KEY -out $PRIVATE_DER -nocrypt
openssl rsa -in $PRIVATE_KEY -pubout -outform DER -out $PUBLIC_DER

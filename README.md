# AUG-Environment #
This a vagrant environment that will create an environment to run the
aug site. If you have the met environment, you can have the website up
and running by cloning this repo, running `vagrant up`, ssh-ing into
the box and running `gulp serve-dev`. You now have a postgres install
with the correct users, you have a java server running in tomcat
handling auth and the angular spa wrapped in angular and running on an
express 4 server. Congratulations, the world is now your oyster.

## Dependencies ##
- Virtual Box
- Vagrant

We are creating a virtual machine and therefore we need a
hypervisor. Unless you have an preference, and the accompanying
expertise to override the default, you will need to install
VirtualBox. Then, install Vagrant, a virtual machine provider to
manage everything. Vagrant allows for provisioning to happen by giving
a script rather than a 400 mb image. You will create the machine from
scratch from these instructions.

## Instructions ##
Clone this repo, and then type `vagrant up`. You are now provisioning
your machine. This will take a while the first time, as you will be
downloading Linux, postgres, java jdk, tomcat, node, and the two repos
that we are interested in. However, Vagrant is smart enough to keep
the linux image cached for future use. So if you want to throw away
the virtual machine, you can easily reprovision it in the future.

To get the website running, type the command `vagrant ssh`. You have
now ssh-ed into the machine and are running in a known and setup
environment.

Pending some accepted pull changes to aug-spa, at this point you can
`cd aug-spa` and run gulp commands here.

- `gulp serve-dev` will run a development server
- `gulp build` will concatenate, minify, vet, and bundle the website
  for production
- `gulp serve-specs` will serve a webpage that will display all unit
  tests and their results. Code changes will trigger a page refresh

## First Time Instructions ##
Since the database is freshly installed, you must run any create
scripts that are required. At this point, this is just the user_auth
create.sql script. To do so, navigate to
`/vagrant/aug-auth/src/main/resources` and we will need to run the
create.sql script there by running the following:
`PGUSER=auth PGPASSWORD=auth psql -h localhost -p 5432 auth -f
create.sql`

This will create the table, but will obviously error if the table
already exists.

You now need to deploy the java auth app. To do this, navigate to the
root of the auth app (/vagrant/aug-auth) and run `./deploy.sh`. This
will run the maven build script. The service should now be running.

Now, navigate to `/vagrant/aug-spa` and run `npm install`. This will
isntall all of the dependencies for the node build chain. After this
process, running `gulp serve-dev` will serve the webapp. View it from
your host computer at localhost:3000.

### Problems ###
The repositories have been a little spotty, so not all packages have
downloaded, or fully so. To fix this, if things are not working well,
go through the list of packages that we need and manually run `sudo
apt-get install -y [package]`. I have found tomcat7 and jdk not fully
installing, which will certainly be a problem.

#### Things to check #### Make sure that the permissions of maven's
config files are correct. They should be owned by vagrant (run `ls
-al~` to see ownership). If they are not, change the owner with `sudo
chown -R vagrant:vagrant ~/.m2`. If you do change this, you need to
restart maven to get it to reload it. I'm not sure how to do this so
just exit vagrant and run `vagrant reload`.

If tomcat was not correctly set up, check and make sure that the file
at `/etc/tomcat7/tomcat-users.xml` is exactly like that in
`/vagrant`. An easy way to do this is `sudo diff
/etc/tomcat7/tomcat-users.xml /vagrant/tomcat-users.xml`. This should
hopefully spit out nothing. If you get output, it means there are
differences. If so, just copy the tomcat users files over to
etc/tomcat7 and then you must restart tomcat. Tomcat is run as a
service so just do `sudo service tomcat7 restart` and you should be
good.


## What the provision scripts do ##
The provision scripts will setup postgres, tomcat, maven,
node, git, mercurial and the jdk.

There are several setup files that are copied to the correct place to
enable maven building and CORS between the javascript express server
and the tomcat java auth backend. Finally, the two projects aug-spa
and aug-auth are cloned if they are not already present.

# TODO/Improvements #
- Have inital box setup run all create scripts automatically
